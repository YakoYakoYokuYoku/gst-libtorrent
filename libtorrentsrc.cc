/*
 * GStreamer
 * Copyright (C) 2023 Martin Rodriguez Reboredo <yakoyoku@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "gstlibtorrentelements.h"
#include "gstlibtorrenterror.h"
#include "libtorrentsrc.h"

#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/replace.hpp>

#include <libtorrent/add_torrent_params.hpp>
#include <libtorrent/extensions/smart_ban.hpp>
#include <libtorrent/extensions/ut_metadata.hpp>
#include <libtorrent/extensions/ut_pex.hpp>
#include <libtorrent/file_storage.hpp>
#include <libtorrent/hex.hpp>
#include <libtorrent/magnet_uri.hpp>
#include <libtorrent/mmap_disk_io.hpp>
#include <libtorrent/read_resume_data.hpp>
#include <libtorrent/session.hpp>
#include <libtorrent/settings_pack.hpp>
#include <libtorrent/torrent_info.hpp>
#include <libtorrent/torrent.hpp>
#include <libtorrent/write_resume_data.hpp>

#include <gst/base/gstdataqueue.h>
#include <gst/base/gsttypefindhelper.h>

#include <algorithm>
#include <array>
#include <bitset>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <thread>
#include <vector>

using namespace std::string_literals;

struct GstLibtorrentBuffer {
  GstMiniObject mini;
  boost::shared_array<gchar> buffer;
  lt::piece_index_t piece;

  GstLibtorrentBuffer (const boost::shared_array<gchar> & buf, const lt::piece_index_t & p);
};

struct GstLibtorrentBufferDeleter {
  void operator() (GstLibtorrentBuffer * buf)
  {
    gst_mini_object_unref (GST_MINI_OBJECT_CAST (buf));
  }
};

typedef std::unique_ptr<GstLibtorrentBuffer, GstLibtorrentBufferDeleter> GstLibtorrentBufferPtr;

struct GstLibtorrentBufferCompare {
  bool operator() (const GstLibtorrentBufferPtr & lhs, const GstLibtorrentBufferPtr & rhs) const
  {
    return lhs->piece < rhs->piece;
  }
};

struct _GstLibtorrentSession {
  lt::session ses;
  lt::torrent_handle handle;
  std::shared_ptr<const lt::torrent_info> info;
  lt::index_range<lt::piece_index_t> cur_range;
  GstDataQueue * data_queue;
  std::set<GstLibtorrentBufferPtr, GstLibtorrentBufferCompare> data_sort;

  lt::string_view location;
  lt::string_view file_path;
  std::string info_hash;
  lt::file_index_t file_index;

  lt::peer_request first_piece;
  lt::peer_request last_piece;
  lt::peer_request cur_piece;
  lt::piece_index_t cur_index;
  GstLibtorrentBufferPtr cur_buffer;
  GstLibtorrentBufferPtr prev;

  GError *error;
  guint npieces;
  guint rpieces;
  bool queueing;

  _GstLibtorrentSession (const lt::session_params &params)
    : ses (params)
    , handle ()
    , info ()
    , data_queue (nullptr)
    , data_sort ()
    , location ()
    , file_path ()
    , info_hash ()
    , file_index (-1)
    , first_piece ()
    , last_piece ()
    , cur_piece ()
    , cur_index ()
    , cur_buffer ()
    , prev ()
    , error (nullptr)
    , rpieces (0)
    , queueing (false)
  {
  }
};

GST_DEFINE_MINI_OBJECT_TYPE (GstLibtorrentBuffer, gst_libtorrent_buffer);

static void
_gst_libtorrent_buffer_free (GstLibtorrentBuffer * buffer)
{
  delete buffer;
}

GstLibtorrentBuffer::GstLibtorrentBuffer (const boost::shared_array<gchar> & buf, const lt::piece_index_t & p)
  : mini()
  , buffer (buf)
  , piece (p)
{
  gst_mini_object_init (GST_MINI_OBJECT_CAST (this), 0,
      gst_libtorrent_buffer_get_type (), nullptr, nullptr,
      (GstMiniObjectFreeFunction) _gst_libtorrent_buffer_free);
}

GST_DEBUG_CATEGORY_STATIC (libtorrentsrc_debug);
#define GST_CAT_DEFAULT libtorrentsrc_debug

static GstStaticPadTemplate srctemplate = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS_ANY);

enum
{
  FILE_LIST,
  LAST_SIGNAL
};

enum
{
  PROP_0,
  PROP_LOCATION,
  PROP_USER_AGENT,
  PROP_FILE_PATH,
  PROP_SAVE_PATH,
  PROP_LISTEN_URL,
};

static std::array<guint, LAST_SIGNAL> gst_libtorrent_src_signals;

#define DEFAULT_USER_AGENT          "GStreamer libtorrentsrc {VERSION} "

static void gst_libtorrent_src_uri_handler_init (gpointer g_iface,
    gpointer iface_data);
static void gst_libtorrent_src_finalize (GObject * gobject);
static void gst_libtorrent_src_dispose (GObject * gobject);

static void gst_libtorrent_src_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_libtorrent_src_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

static GstStateChangeReturn gst_libtorrent_src_change_state (GstElement *
    element, GstStateChange transition);
static void gst_libtorrent_src_set_context (GstElement * element,
    GstContext * context);
static GstFlowReturn gst_libtorrent_src_create (GstPushSrc * psrc,
    GstBuffer ** outbuf);
static gboolean gst_libtorrent_src_start (GstBaseSrc * bsrc);
static gboolean gst_libtorrent_src_stop (GstBaseSrc * bsrc);
static gboolean gst_libtorrent_src_get_size (GstBaseSrc * bsrc, guint64 * size);
static gboolean gst_libtorrent_src_is_seekable (GstBaseSrc * bsrc);
static gboolean gst_libtorrent_src_do_seek (GstBaseSrc * bsrc,
    GstSegment * segment);
static gboolean gst_libtorrent_src_query (GstBaseSrc * bsrc, GstQuery * query);
// static gboolean gst_libtorrent_src_unlock (GstBaseSrc * bsrc);
// static gboolean gst_libtorrent_src_unlock_stop (GstBaseSrc * bsrc);
// static gboolean gst_libtorrent_src_set_location (GstLibtorrentSrc * src,
//     const gchar * uri, GError ** error);
static gpointer gst_libtorrent_src_loop_task (GstLibtorrentSrc * src);
static gboolean gst_libtorrent_src_session_open (GstLibtorrentSrc * src);
// static void gst_libtorrent_src_session_close (GstLibtorrentSrc * src);
static void gst_libtorrent_src_drop_until (GstLibtorrentSession *session, lt::piece_index_t p);

#define gst_libtorrent_src_parent_class parent_class
G_DEFINE_TYPE_WITH_CODE (GstLibtorrentSrc, gst_libtorrent_src, GST_TYPE_PUSH_SRC,
    G_IMPLEMENT_INTERFACE (GST_TYPE_URI_HANDLER,
        gst_libtorrent_src_uri_handler_init));

static gboolean libtorrentsrc_element_init (GstPlugin * plugin);
GST_ELEMENT_REGISTER_DEFINE_CUSTOM (libtorrentsrc, libtorrentsrc_element_init);

static void
gst_libtorrent_src_class_init (GstLibtorrentSrcClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;
  GstBaseSrcClass *gstbasesrc_class;
  GstPushSrcClass *gstpushsrc_class;

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;
  gstbasesrc_class = (GstBaseSrcClass *) klass;
  gstpushsrc_class = (GstPushSrcClass *) klass;

  gobject_class->set_property = gst_libtorrent_src_set_property;
  gobject_class->get_property = gst_libtorrent_src_get_property;
  gobject_class->finalize = gst_libtorrent_src_finalize;
  gobject_class->dispose = gst_libtorrent_src_dispose;

  g_object_class_install_property (gobject_class,
      PROP_LOCATION,
      g_param_spec_string ("location", "Location",
          "Location to read from (either a .torrent file or a magnet URI)", "",
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));
  g_object_class_install_property (gobject_class,
      PROP_USER_AGENT,
      g_param_spec_string ("user-agent", "User-Agent",
          "Value of the User-Agent of the client to the tracker",
          DEFAULT_USER_AGENT,
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));
  g_object_class_install_property (gobject_class,
      PROP_FILE_PATH,
      g_param_spec_string ("file-path", "File Path",
          "Path to torrent file member", "",
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));
  g_object_class_install_property (gobject_class,
      PROP_SAVE_PATH,
      g_param_spec_string ("save-path", "Save Path",
          "Output directory to write files", ".",
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));
  g_object_class_install_property (gobject_class,
      PROP_LISTEN_URL,
      g_param_spec_string ("listen-url", "Listen URL",
          "URL for listening torrents", "",
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  gst_libtorrent_src_signals[FILE_LIST] =
    g_signal_new ("file-list", G_TYPE_FROM_CLASS (klass),
    G_SIGNAL_RUN_FIRST, G_STRUCT_OFFSET (GstLibtorrentSrcClass, file_list), nullptr,
    nullptr, nullptr, G_TYPE_NONE, 1, G_TYPE_STRV);

  gst_element_class_add_static_pad_template (gstelement_class, &srctemplate);

  gst_element_class_set_static_metadata (gstelement_class, "BitTorrent peer source",
      "Source/Network",
      "Stream multimedia via BitTorrent using libtorrent",
      "Martin Rodriguez Reboredo <yakoyoku@gmail.com>");
  gstelement_class->change_state =
      GST_DEBUG_FUNCPTR (gst_libtorrent_src_change_state);
  gstelement_class->set_context =
      GST_DEBUG_FUNCPTR (gst_libtorrent_src_set_context);

  gstbasesrc_class->start = GST_DEBUG_FUNCPTR (gst_libtorrent_src_start);
  gstbasesrc_class->stop = GST_DEBUG_FUNCPTR (gst_libtorrent_src_stop);
  // gstbasesrc_class->unlock = GST_DEBUG_FUNCPTR (gst_libtorrent_src_unlock);
  // gstbasesrc_class->unlock_stop =
  //     GST_DEBUG_FUNCPTR (gst_libtorrent_src_unlock_stop);
  gstbasesrc_class->get_size = GST_DEBUG_FUNCPTR (gst_libtorrent_src_get_size);
  gstbasesrc_class->is_seekable =
      GST_DEBUG_FUNCPTR (gst_libtorrent_src_is_seekable);
  gstbasesrc_class->do_seek = GST_DEBUG_FUNCPTR (gst_libtorrent_src_do_seek);
  gstbasesrc_class->query = GST_DEBUG_FUNCPTR (gst_libtorrent_src_query);

  gstpushsrc_class->create = GST_DEBUG_FUNCPTR (gst_libtorrent_src_create);
}

static void
gst_libtorrent_src_reset (GstLibtorrentSrc * src)
{
  g_timer_stop (src->resume_file_timer);
  src->content_size = 0;
  src->piece_length = 0;
  src->read_position = 0;
  //src->request_position = 0;
  src->stop_position = GST_CLOCK_TIME_NONE;
}

static void
gst_libtorrent_src_init (GstLibtorrentSrc * src)
{
  src->resume_file_timer = g_timer_new ();

  g_mutex_init (&src->session_mutex);
  g_cond_init (&src->session_cond);
  g_rec_mutex_init (&src->task_mutex);
  g_mutex_init (&src->cmd_mutex);
  g_cond_init (&src->cmd_cond);

  src->location = nullptr;
  src->user_agent = g_strdup(DEFAULT_USER_AGENT);
  src->save_path = g_strdup(".");

  src->file_list = nullptr;

  src->task = nullptr;

  gst_libtorrent_src_reset (src);
}

static void
gst_libtorrent_src_dispose (GObject * gobject)
{
  GstLibtorrentSrc *src = GST_LIBTORRENT_SRC (gobject);

  GST_DEBUG_OBJECT (src, "dispose");

  G_OBJECT_CLASS (parent_class)->dispose (gobject);
}

static void
gst_libtorrent_src_finalize (GObject * gobject)
{
  GstLibtorrentSrc *src = GST_LIBTORRENT_SRC (gobject);

  GST_DEBUG_OBJECT (src, "finalize");

  if (src->session)
    src->session->ses.pause ();

  g_timer_destroy (src->resume_file_timer);

  if (src->task)
    gst_task_join (src->task);
  g_mutex_clear (&src->session_mutex);
  g_cond_clear (&src->session_cond);
  g_rec_mutex_clear (&src->task_mutex);
  g_mutex_clear (&src->cmd_mutex);
  g_cond_clear (&src->cmd_cond);

  g_free (src->location);
  g_free (src->user_agent);
  g_free (src->save_path);

  g_strfreev (src->file_list);

  G_OBJECT_CLASS (parent_class)->finalize (gobject);
}

static void
gst_libtorrent_src_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstLibtorrentSrc *src = GST_LIBTORRENT_SRC (object);

  switch (prop_id) {
    case PROP_LOCATION:
      if (!g_value_get_string (value)) {
        g_warning ("location property cannot be NULL");
        break;
      }
      g_free (src->location);
      src->location = g_value_dup_string (value);
      if (src->session)
        src->session->location = lt::string_view (src->location);
      break;
    case PROP_USER_AGENT:
      g_free (src->user_agent);
      src->user_agent = g_value_dup_string (value);
      if (src->session) {
        std::string user_agent (src->user_agent ? : "");
        boost::replace_all (user_agent, "{VERSION}", /*PACKAGE_VERSION*/"0.0.1");
        if (user_agent.empty () || user_agent.back () == ' ')
          user_agent += "libtorrent/"s + lt::version ();
        lt::settings_pack p (src->session->ses.get_settings ());
        p.set_str (lt::settings_pack::user_agent, user_agent);
        src->session->ses.apply_settings (p);
      }
      break;
    case PROP_FILE_PATH:
      if (!g_value_get_string (value)) {
        g_warning ("file-path property cannot be NULL");
        break;
      }
      g_free (src->file_path);
      src->file_path = g_value_dup_string (value);
      if (src->session)
        src->session->file_path = lt::string_view (src->file_path);
      break;
    case PROP_SAVE_PATH:
      if (!g_value_get_string (value)) {
        g_warning ("save-path property cannot be NULL");
        break;
      }
      g_free (src->save_path);
      src->save_path = g_value_dup_string (value);
      break;
    case PROP_LISTEN_URL:
      if (!g_value_get_string (value)) {
        g_warning ("listen-url property cannot be NULL");
        break;
      }
      g_free (src->listen_url);
      src->listen_url = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_libtorrent_src_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstLibtorrentSrc *src = GST_LIBTORRENT_SRC (object);

  switch (prop_id) {
    case PROP_LOCATION:
      g_value_set_string (value, src->location);
      break;
    case PROP_USER_AGENT:
      g_value_set_string (value, src->user_agent);
      break;
    case PROP_FILE_PATH:
      g_value_set_string (value, src->file_path);
      break;
    case PROP_SAVE_PATH:
      g_value_set_string (value, src->save_path);
      break;
    case PROP_LISTEN_URL:
      g_value_set_string (value, src->listen_url);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static const std::array subtitle_mimes = {
  "application/x-ass"s,
  "application/x-ssa"s,
  "application/x-subtitle"s,
  "application/x-ttml+xml"s,
  "subtitle/x-kate"s,
};

struct GstLibtorrentIsSub {
  lt::string_view mime;
  GstLibtorrentIsSub (lt::string_view media_type) : mime (media_type) {}
  bool operator() (const std::string &s) const { return mime.starts_with (s); }
};

// Returns true if a cap describing an image was passed, false otherwise.
static gint
gst_libtorrent_src_caps_func (GstCapsFeatures * features,
    GstStructure *structure, gpointer data)
{
  lt::string_view media_type (gst_structure_get_name (structure));
  GST_DEBUG_OBJECT (reinterpret_cast<GstLibtorrentSrc *> (data),
      "caps_func(): %s", gst_structure_get_name (structure));
  return media_type.starts_with ("image/") ||
      std::any_of (subtitle_mimes.cbegin (), subtitle_mimes.cend (),
          GstLibtorrentIsSub (media_type));
}

static gboolean
gst_libtorrent_src_choose_torrent_file (GstLibtorrentSrc * src)
{
  const lt::file_storage &files (src->session->info->files ());
  bool found = false;
  std::vector<lt::download_priority_t> file_prios (src->session->handle.get_file_priorities ());

  if (src->file_path) {
    for (lt::file_index_t f : files.file_range ()) {
      if (files.file_path (f) == src->session->file_path) {
        src->session->file_index = f;
        std::fill(file_prios.begin (), file_prios.end (), lt::dont_download);
        found = true;
        break;
      }
    }
  }

  if (!found) {
    std::vector<lt::download_priority_t>::const_iterator f =
        std::find_if (file_prios.cbegin (), file_prios.cend (), [](const lt::download_priority_t p) { return p != lt::dont_download; });
    if (f != file_prios.cend ()) {
      src->session->file_index = f - file_prios.cbegin ();
      g_free (src->file_path);
      src->file_path = g_strdup (files.file_path (src->session->file_index).c_str ());
      found = true;
    }
    std::fill(file_prios.begin (), file_prios.end (), lt::dont_download);
  }

  if (!found) {
    std::string res, img;
    lt::file_index_t res_index = -1, img_index = -1;
    for (lt::file_index_t f : files.file_range ()) {
      GstCaps *caps;

      std::string file_path (files.file_path (f));
      std::string::size_type dot = file_path.rfind (".");
      if (dot == std::string::npos) continue;

      std::string extension (file_path.substr (dot + 1));

      caps = gst_type_find_helper_for_extension (GST_OBJECT (src), extension.data ());
      if (!caps) continue;

      // Because a torrent is one file or many then it is likely that
      // it'll be one cap foreach.
      if (gst_caps_foreach (caps, gst_libtorrent_src_caps_func, src)) {
        img = std::move (file_path);
        img_index = f;
      } else {
        res = std::move (file_path);
        res_index = f;
        break;
      }
    }

    if (res.empty ()) {
      if (img.empty ()) {
        GST_WARNING_OBJECT (src, "choose_torrent_file() couldn't choose a file");
        return FALSE;
      }
      res = img;
      res_index = img_index;
    }

    g_free (src->file_path);
    src->file_path = g_strdup (res.c_str ());
    if (src->session)
      src->session->file_path = lt::string_view (src->file_path);
    src->session->file_index = res_index;
  }

  GST_DEBUG_OBJECT (src, "choose_torrent_file() = %s", src->file_path);

  src->session->handle.prioritize_files (file_prios);
  file_prios[src->session->file_index] = lt::top_priority;
  src->session->handle.prioritize_files (file_prios);

  std::vector<lt::download_priority_t> piece_prios (src->session->handle.get_piece_priorities ());
  std::fill(piece_prios.begin (), piece_prios.end (), lt::dont_download);
  src->session->handle.prioritize_pieces (piece_prios);

  src->content_size = files.file_size (src->session->file_index);
  src->session->first_piece = files.map_file (src->session->file_index, 0, std::min (src->content_size, src->piece_length));
  src->session->first_piece.length -= src->session->first_piece.start;
  src->session->last_piece = files.map_file (src->session->file_index, src->content_size, 0);
  src->session->last_piece.length = (src->content_size + src->session->first_piece.start) % src->piece_length;
  src->session->cur_piece = src->session->first_piece;
  src->session->cur_range = {src->session->first_piece.piece, src->session->last_piece.piece};

  return TRUE;
}

static gboolean
gst_libtorrent_queue_full (GstDataQueue * queue, guint visible, guint bytes, guint64 time, gpointer checkdata)
{
  //GstLibtorrentSrc *src = (GstLibtorrentSrc *) checkdata;

  return visible <= 40;
}

static const std::array default_trackers = {
  "# Tier 0"s,
  "http://bt.t-ru.org/ann"s,
  "http://bt2.t-ru.org/ann"s,
  "http://bt3.t-ru.org/ann"s,
  "http://bt4.t-ru.org/ann"s,
  "http://92.42.108.54:2710/announce"s,
  "http://tracker.grepler.com:6969/announce"s,
  "http://bt1.archive.org:6969/announce"s,
  "http://bt2.archive.org:6969/announce"s,
  "http://sugoi.pomf.se/announce"s,
  "http://tracker.pomf.se/announce"s,

  "# Tier 1"s,
  "udp://tracker.opentrackr.org:1337/announce"s,
  "udp://tracker.openbittorrent.com:80/announce"s,
  "udp://explodie.org:6969"s,
  "udp://opentor.org:2710"s,
  "udp://valakas.rollo.dnsabr.com:2710"s,
  "udp://retracker.lanta-net.ru:2710"s,
  "udp://9.rarbg.me:2740"s,
  "udp://9.rarbg.to:2790"s,
  "udp://tracker.uw0.xyz:6969"s,
  "udp://open.stealth.si:80"s,
  "udp://exodus.desync.com:6969"s,
  "udp://tracker.theoks.net:6969"s,
  "udp://tracker.moeking.me:6969"s,
  "udp://tracker.torrent.eu.org:451"s,
  "udp://tracker.ololosh.space:6969"s,
  "udp://t1.leech.ie:1337"s,
  "udp://inferno.demonoid.is:3391"s,
  "udp://mts.tvbit.co:6969"s,
  "udp://retracker.netbynet.ru:2710"s,
  "udp://edu.uifr.ru:6969"s,
  "udp://engplus.ru:6969"s,
  "udp://torrent.by:2710"s,
  "udp://tracker.cortexlabs.ai:5008"s
};

static lt::add_torrent_params
gst_libtorrent_src_add_torrent_params (GstLibtorrentSrc * src)
{
  lt::add_torrent_params ret;

  if (src->session->location.starts_with ("magnet")) {
    lt::error_code ec;
    ret = lt::parse_magnet_uri (src->location, ec);
  } else {
    ret.ti = std::make_shared<lt::torrent_info> (src->location);
  }

  if (src->save_path && *src->save_path)
    ret.save_path = src->save_path;

  if (ret.info_hashes.has_v2())
    src->session->info_hash = lt::aux::to_hex(ret.info_hashes.v2);
  else
    src->session->info_hash = lt::aux::to_hex(ret.info_hashes.v1);
  std::string resume_path (ret.save_path + "/." + src->session->info_hash + ".fastresume");
  std::ifstream resume_stream (resume_path.c_str(), std::ios::binary);
  if (resume_stream) {
    std::vector<char> resume_data;
    std::copy (std::istreambuf_iterator<char> (resume_stream), std::istreambuf_iterator<char> (),
        std::back_inserter(resume_data));
    lt::error_code ec;
    lt::add_torrent_params resumed (lt::read_resume_data (resume_data, ec));
    if (ec) {
      GST_DEBUG_OBJECT (src, "Resume data is invalid \"%s\": %s", resume_path.c_str (), ec.message ().c_str ());
    } else {
      GST_DEBUG_OBJECT (src, "Read resume data \"%s\"", resume_path.c_str ());
      std::vector<lt::download_priority_t>::const_iterator f =
          std::find_if (ret.file_priorities.cbegin (), ret.file_priorities.cend (),
              [](const lt::download_priority_t p) { return p != lt::dont_download; });
      if (f != ret.file_priorities.end ()) {
        resumed.file_priorities = ret.file_priorities;
      }
      ret = resumed;
    }
  } else {
    GST_DEBUG_OBJECT (src, "Failed to open resume data \"%s\": %s", resume_path.c_str (), g_strerror (errno));
  }

  ret.flags = lt::torrent_flags::default_flags & ~lt::torrent_flags::auto_managed;

  ret.max_connections = 100;

  //ret.ti = std::make_shared<lt::torrent_info> (ret.save_path + "/." + src->session->info_hash + ".torrent");

  return ret;
}

static void
gst_libtorrent_src_add_trackers (lt::add_torrent_params & p)
{
  std::vector<int>::const_iterator max_tier = std::max_element (p.tracker_tiers.cbegin (), p.tracker_tiers.cend ());
  gint base = (max_tier != p.tracker_tiers.cend ()) ? (*max_tier + 1) : 0;
  gint tier = base;
  p.trackers.reserve (p.trackers.size () + default_trackers.size ());
  p.tracker_tiers.reserve (p.tracker_tiers.size () + default_trackers.size ());
  p.tracker_tiers.resize (p.tracker_tiers.size (), 0);
  for (const std::string &tracker : default_trackers) {
    if (boost::algorithm::starts_with (tracker, "# Tier")) {
      std::istringstream n (tracker);
      n.ignore (6);
      n >> tier;
      tier += base;
      continue;
    }
    p.trackers.emplace_back (tracker);
    p.tracker_tiers.emplace_back (tier);
  }
}

static gboolean
gst_libtorrent_src_download_metadata (GstLibtorrentSrc * src, const lt::add_torrent_params & atp)
{
  GST_INFO_OBJECT (src, "Downloading metadata");

  lt::add_torrent_params p (atp);

  p.save_path = g_get_tmp_dir () + "/."s + src->session->info_hash;

  p.flags &= ~lt::torrent_flags::paused;
  p.flags &= ~lt::torrent_flags::auto_managed;
  p.flags |= lt::torrent_flags::upload_mode;

  src->session->ses.async_add_torrent (std::move (p));

  lt::torrent_handle handle;

  while (!src->session->info) {
    std::vector<lt::alert *> alerts;
    src->session->ses.pop_alerts (&alerts);

    for (const lt::alert *a : alerts) {
      GST_TRACE_OBJECT (src, "alerts = %s", a->message ().c_str ());
      if (a->category () & lt::alert_category::error) {
        auto tracker_error = lt::alert_cast<lt::tracker_error_alert> (a);
        auto portmap_error = lt::alert_cast<lt::portmap_error_alert> (a);
        auto metadata_failed = lt::alert_cast<lt::metadata_failed_alert> (a);
        if (tracker_error) {
          //src->session->handle.force_reannounce ();
          GST_DEBUG_OBJECT (src, "Tracker error with %s \"%s\" %i",
              tracker_error->tracker_url (),
              tracker_error->error.message ().c_str (), tracker_error->error.value ());
          if (tracker_error->times_in_row > 2) {
            handle.force_reannounce ();
            continue;
          }
        } else if (portmap_error) {
          //GST_DEBUG_OBJECT (src, "Portmap error \"%s\" %i",
          //    portmap_error->error.message ().c_str (), portmap_error->error.value ());
        } else if (metadata_failed) {
          GST_DEBUG_OBJECT (src, "Metadata corruption \"%s\" %i",
              metadata_failed->error.message ().c_str (), metadata_failed->error.value ());
        } else {
          GST_ELEMENT_ERROR (src, RESOURCE, OPEN_READ, ("Missing metadata."),
              ("Alert message \"%s\"", a->message ().c_str ()));
          return FALSE;
        }
      }

      auto add_torrent = lt::alert_cast<lt::add_torrent_alert> (a);
      if (add_torrent) {
        GST_DEBUG_OBJECT (src, "Metadata torrent");
        handle = add_torrent->handle;
      }

      auto metadata_received = lt::alert_cast<lt::metadata_received_alert> (a);
      if (metadata_received) {
        GST_DEBUG_OBJECT (src, "Metadata received");
        src->session->info = metadata_received->handle.torrent_file ();
        src->session->ses.remove_torrent (metadata_received->handle, lt::session::delete_files);
        break;
      }

      auto tracker_reply = lt::alert_cast<lt::tracker_reply_alert> (a);
      if (tracker_reply) {
        GST_LOG_OBJECT (src, "Tracker replied %s",
            tracker_reply->tracker_url ());
      }
    }

    if (!src->session->info) {
      g_usleep (100000);
    }
  }

  GST_INFO_OBJECT (src, "Downloaded metadata");

  return TRUE;
}

static void
gst_libtorrent_src_list_files (GstLibtorrentSrc * src)
{
  const lt::file_storage &files (src->session->info->files ());
  GStrvBuilder * builder = g_strv_builder_new ();

  for (lt::file_index_t f : files.file_range ()) {
    g_strv_builder_add (builder, files.file_path (f).c_str ());
  }

  src->file_list = g_strv_builder_end (builder);
  g_strv_builder_unref (builder);

  g_signal_emit (src, gst_libtorrent_src_signals[FILE_LIST], 0, src->file_list);
}

static const std::string default_dht_nodes = 
  "dht.libtorrent.org:25401, "
  "router.utorrent.com:6881, "
  "router.bittorrent.com:6881, "
  "dht.transmissionbt.com:6881, "
  "router.silotis.us:6881"s;

static const lt::alert_category_t default_categories =
  lt::alert_category::status |
  lt::alert_category::error |
  lt::alert_category::port_mapping |
  lt::alert_category::storage |
  lt::alert_category::tracker |
  lt::alert_category::dht |
  lt::alert_category::piece_progress;

static gboolean
gst_libtorrent_src_session_open (GstLibtorrentSrc * src)
{
  if (src->session) {
    GST_DEBUG_OBJECT (src, "Session is already open");
    return TRUE;
  }

  if (!src->location) {
    GST_ELEMENT_ERROR (src, RESOURCE, OPEN_READ, ("No torrent file or URI set."),
        ("Missing location property"));
    return FALSE;
  }

  std::string user_agent (src->user_agent ? : "");
  boost::replace_all (user_agent, "{VERSION}", /* PACKAGE_VERSION */"0.0.1");
  if (user_agent.empty () || user_agent.back () == ' ')
    user_agent += "libtorrent/"s + lt::version ();

  lt::settings_pack p;

  //p.set_str (lt::settings_pack::peer_fingerprint, lt::generate_fingerprint ("qB", 5, 0, 0));

  p.set_str (lt::settings_pack::user_agent, user_agent);

  p.set_int (lt::settings_pack::alert_mask, default_categories);

  p.set_int (lt::settings_pack::connections_limit, 500);

  // Trackers
  p.set_bool (lt::settings_pack::announce_to_all_tiers, true);
  p.set_bool (lt::settings_pack::announce_to_all_trackers, false);
  p.set_bool (lt::settings_pack::apply_ip_filter_to_trackers, false);
  p.set_bool (lt::settings_pack::no_atime_storage, true);
  p.set_bool (lt::settings_pack::rate_limit_ip_overhead, false);
  p.set_bool (lt::settings_pack::upnp_ignore_nonrouters, true);
  p.set_bool (lt::settings_pack::use_parole_mode, false);
  //p.set_bool (lt::settings_pack::disable_hash_checks, true);

  // Connections
  p.set_int (lt::settings_pack::mixed_mode_algorithm, lt::settings_pack::prefer_tcp);
  p.set_int (lt::settings_pack::upnp_lease_duration, 0);
  p.set_bool (lt::settings_pack::enable_incoming_tcp, true);
  p.set_bool (lt::settings_pack::enable_outgoing_tcp, true);
  p.set_bool (lt::settings_pack::enable_incoming_utp, true);
  p.set_bool (lt::settings_pack::enable_outgoing_utp, true);
  p.set_int (lt::settings_pack::active_checking, 1);
  p.set_int (lt::settings_pack::active_downloads, -1);
  p.set_int (lt::settings_pack::active_seeds, -1);
  p.set_int (lt::settings_pack::active_limit, -1);
  p.set_int (lt::settings_pack::active_tracker_limit, -1);
  p.set_int (lt::settings_pack::active_dht_limit, -1);
  p.set_int (lt::settings_pack::active_lsd_limit, -1);

  // Crypto
  p.set_int (lt::settings_pack::allowed_enc_level, lt::settings_pack::pe_rc4);
  p.set_bool (lt::settings_pack::prefer_rc4, true);

  // Services
  p.set_bool (lt::settings_pack::enable_upnp, false);
  p.set_bool (lt::settings_pack::enable_natpmp, false);
  p.set_bool (lt::settings_pack::enable_dht, true);
  p.set_bool (lt::settings_pack::enable_lsd, false);
  p.set_bool (lt::settings_pack::listen_system_port_fallback, false);
  p.set_bool (lt::settings_pack::use_dht_as_fallback, false);

  // I/O
  p.set_bool (lt::settings_pack::enable_set_file_valid_data, true);
  p.set_int (lt::settings_pack::aio_threads, std::max(1, int(std::thread::hardware_concurrency())) * 4);
  p.set_int (lt::settings_pack::mixed_mode_algorithm, lt::settings_pack::prefer_tcp);

  // Intervals
  p.set_int (lt::settings_pack::min_announce_interval, 30);
  p.set_int (lt::settings_pack::dht_announce_interval, 60);
  p.set_int (lt::settings_pack::stop_tracker_timeout, 1);

  // Sizes
  p.set_int (lt::settings_pack::request_queue_time, 2);
  p.set_int (lt::settings_pack::max_out_request_queue, 5000);
  p.set_int (lt::settings_pack::max_allowed_in_request_queue, 5000);
  p.set_int (lt::settings_pack::max_peerlist_size, 50000);
  p.set_int (lt::settings_pack::dht_upload_rate_limit, 50000);
  p.set_int (lt::settings_pack::max_pex_peers, 200);
  p.set_int (lt::settings_pack::max_suggest_pieces, 50);
  p.set_int (lt::settings_pack::whole_pieces_threshold, 10);
  p.set_int (lt::settings_pack::connection_speed, 300);

  p.set_str (lt::settings_pack::dht_bootstrap_nodes, default_dht_nodes);

  std::ostringstream listen_interfaces;

  if (src->listen_url) {
    listen_interfaces << src->listen_url;
  } else {
    listen_interfaces << "0.0.0.0:" << 6881 << ",[::]:" << 6881;
    for (std::string::size_type i = 6882; i <= 6889; ++i) {
      listen_interfaces << ",0.0.0.0:" << i << ",[::]:" << i;
    }
  }

  p.set_str (lt::settings_pack::listen_interfaces, listen_interfaces.str ());

  lt::session_params params (std::move(p));

//#if TORRENT_HAVE_MMAP || TORRENT_HAVE_MAP_VIEW_OF_FILE
//  params.disk_io_constructor = lt::mmap_disk_io_constructor;
//#endif

  src->session = new GstLibtorrentSession (params);
  src->session->ses.pause ();
  src->session->location = lt::string_view (src->location);
  src->session->data_queue = gst_data_queue_new (gst_libtorrent_queue_full, nullptr, nullptr, src);

  src->session->ses.add_extension(&lt::create_smart_ban_plugin);
  src->session->ses.add_extension(&lt::create_ut_metadata_plugin);
  if (true /*src->pex_enabled*/)
    src->session->ses.add_extension(&lt::create_ut_pex_plugin);

  //src->session->ses.add_port_mapping (lt::portmap_protocol::tcp, 6881, 6881);
  //src->session->ses.add_port_mapping (lt::portmap_protocol::udp, 6881, 6881);

  p = src->session->ses.get_settings ();
  p.set_bool (lt::settings_pack::enable_upnp, true);
  p.set_bool (lt::settings_pack::enable_natpmp, true);
  src->session->ses.apply_settings (p);

  lt::add_torrent_params atp (gst_libtorrent_src_add_torrent_params (src));

  gst_libtorrent_src_add_trackers (atp);
  src->session->info = atp.ti;

  src->session->ses.resume ();

  if (!atp.ti)
    gst_libtorrent_src_download_metadata (src, atp);

  if (!src->session->info)
    return FALSE;

  gst_libtorrent_src_list_files (src);

  if (!atp.ti)
    atp.ti = std::make_shared<lt::torrent_info> (*src->session->info);
  src->session->handle = src->session->ses.add_torrent (atp);

  src->piece_length = src->session->info->piece_length ();

  GST_DEBUG_OBJECT (src, "open() = %s", src->session->info_hash.c_str ());

  if (!gst_libtorrent_src_choose_torrent_file (src)) {
    GST_ELEMENT_ERROR (src, RESOURCE, OPEN_READ, ("File was not selected."),
        ("Missing file-path property"));
    return FALSE;
  }

  if (src->session->ses.is_paused ()) {
    GST_DEBUG_OBJECT (src, "open() session was paused");
    src->session->ses.resume ();
  }

  if (src->session->handle.flags () & lt::torrent_flags::paused) {
    GST_DEBUG_OBJECT (src, "open() handle was paused");
    src->session->handle.resume ();
  }

  std::vector<lt::download_priority_t> piece_prios (src->session->handle.get_piece_priorities ());
  std::fill(piece_prios.begin (), piece_prios.end (), lt::dont_download);
  src->session->handle.prioritize_pieces (piece_prios);

  atp.ti = std::const_pointer_cast<lt::torrent_info> (src->session->info);
  std::ofstream torrent_file (
      (atp.save_path + "/." + src->session->info_hash + ".torrent").c_str (),
      std::ios_base::binary);
  std::vector<char> buf = lt::write_torrent_file_buf (atp, {});
  torrent_file.write (buf.data(), buf.size());

  src->task = gst_task_new (reinterpret_cast<GstTaskFunction> (gst_libtorrent_src_loop_task), src, nullptr);
  gst_task_set_lock (src->task, &src->task_mutex);
  gst_task_start (src->task);

  src->session->handle.save_resume_data (lt::torrent_handle::only_if_modified);

  return TRUE;
}

static void
gst_libtorrent_src_insert_buffer (GstLibtorrentSrc * src, lt::piece_index_t piece,
    const boost::shared_array<gchar> & buffer)
{
  if (piece < *src->session->cur_range.begin () ||
      piece >= *src->session->cur_range.end ()) {
    GST_DEBUG_OBJECT (src, "Ignoring out of range piece %u", guint (piece));
    return;
  }
  src->session->data_sort.emplace (new GstLibtorrentBuffer (buffer, piece));
  ++src->session->rpieces;
}

static void
gst_libtorrent_src_read_pieces (GstLibtorrentSrc * src,
    lt::index_range<lt::piece_index_t> piece_range)
{
  GST_DEBUG_OBJECT (src, "Reading pieces %u-%u",
      guint (*piece_range.begin ()), guint (*piece_range.end ()));

  for (lt::piece_index_t p : piece_range) {
    if (std::find_if (src->session->data_sort.cbegin (), src->session->data_sort.cend (),
        [&p](const GstLibtorrentBufferPtr & buf) { return buf->piece == p; }) ==
        src->session->data_sort.cend () ||
        (src->session->cur_buffer && src->session->cur_buffer->piece != p)) {
      lt::download_priority_t prio = p <= (*piece_range.begin () + lt::piece_index_t (10)) ? lt::top_priority : lt::download_priority_t (5);
      src->session->handle.set_piece_deadline (p, (p - *piece_range.begin ()) * 50,
          lt::torrent_handle::alert_when_available);
      src->session->handle.piece_priority (p, prio);
    }
  }
}

static void
gst_libtorrent_src_cancel_pieces (GstLibtorrentSrc * src,
    lt::index_range<lt::piece_index_t> piece_range)
{
  GST_DEBUG_OBJECT (src, "Cancelling pieces %u-%u",
      (guint) (*piece_range.begin ()), (guint) (*piece_range.end ()));

  for (lt::piece_index_t p : piece_range) {
    src->session->handle.piece_priority (p, lt::dont_download);
  }
}

static void
gst_libtorrent_queue_item_free (GstDataQueueItem * item)
{
  if (item->object)
    gst_mini_object_unref (item->object);
  g_free (item);
}

static void
gst_libtorrent_queue_sorted (GstLibtorrentSession * session)
{
  if (session->data_sort.empty ())
    return;

  auto it = session->data_sort.begin ();

  if ((*it)->piece > session->cur_index)
    return;

  while (it != session->data_sort.end () && (*it)->piece < session->cur_index) {
    it = session->data_sort.erase (it);
    --session->rpieces;
  }

  while (it != session->data_sort.end ()) {
    lt::piece_index_t p = (*it)->piece;
    if (p != session->cur_index) {
      GST_DEBUG ("Missing piece %u in data sort, waiting for it", guint (session->cur_index));
      break;
    }
    GstLibtorrentBufferPtr buf (std::move (session->data_sort.extract (it).value ()));
    GstDataQueueItem *item = g_new0 (GstDataQueueItem, 1);
    item->object = GST_MINI_OBJECT (buf.release ());
    item->visible = true;
    item->destroy = reinterpret_cast<GDestroyNotify> (gst_libtorrent_queue_item_free);
    if (!gst_data_queue_push_force (session->data_queue, item)) {
      GST_DEBUG ("Failed to push to data queue");
      gst_libtorrent_queue_item_free (item);
      break;
    }
    it = session->data_sort.begin ();
    ++session->cur_index;
  }
}

static gpointer
gst_libtorrent_src_loop_task (GstLibtorrentSrc * src)
{
  lt::torrent_status s;
  g_timer_start (src->resume_file_timer);

  while (!(s.flags & lt::torrent_flags::paused)) {
    g_mutex_lock (&src->session_mutex);

    GstDataQueueSize level;
    gst_data_queue_get_level (src->session->data_queue, &level);

    if (level.visible == 0 || src->session->data_sort.size () >= 20) {
      level.visible += src->session->data_sort.size ();
      gst_libtorrent_queue_sorted (src->session);
    }

    std::vector<lt::alert *> alerts;
    src->session->ses.pop_alerts (&alerts);

    for (lt::alert *a : alerts) {
      GST_TRACE_OBJECT (src, "alerts = %s", a->message ().c_str ());
      if (a->category () & lt::alert_category::error) {
        /*const lt::tracker_error_alert *tracker_error =
            lt::alert_cast<lt::tracker_error_alert> (a);
        if (tracker_error) {
          if (tracker_error->times_in_row < 10) {
            src->session->handle.force_reannounce ();
            continue;
          }
        }*/
        if (lt::alert_cast<lt::tracker_error_alert> (a) ||
            lt::alert_cast<lt::portmap_error_alert> (a) ||
            lt::alert_cast<lt::save_resume_data_failed_alert> (a)) continue;
        auto dropped = lt::alert_cast<lt::alerts_dropped_alert> (a);
        if (dropped && !dropped->dropped_alerts.test (lt::alert_category::storage)) continue;
        g_set_error_literal (&src->session->error,
            GST_LIBTORRENT_ERROR,
            //tracker_error->error.value (),
            //"Tracker error",
            0,
            a->message ().c_str ());
        g_cond_signal (&src->session_cond);
        g_mutex_unlock (&src->session_mutex);

        g_mutex_lock (&src->cmd_mutex);
        g_cond_wait_until (&src->cmd_cond, &src->cmd_mutex, 500000);
        g_mutex_unlock (&src->cmd_mutex);
        return nullptr;
      }

      if (a->category () & lt::alert_category::dht) {
        GST_TRACE_OBJECT (src, "dht = %s", a->message ().c_str ());
      }

      if (a->category () & lt::alert_category::port_mapping) {
        GST_TRACE_OBJECT (src, "port_mapping = %s", a->message ().c_str ());
      }

      switch (a->type ()) {
      case lt::read_piece_alert::alert_type: {
          const auto piece = lt::alert_cast<lt::read_piece_alert> (a);
          GST_LOG_OBJECT (src, "read_piece_alert = %u, %i, %s", (guint) (piece->piece),
              piece->error.value (), piece->message ().c_str ());

          if (!piece->buffer && piece->error) {
            if (src->session->error) break;
            g_set_error_literal (&src->session->error,
                GST_LIBTORRENT_ERROR,
                piece->error.value (),
                piece->error.message ().c_str ());
            break;
          }
          gst_libtorrent_src_insert_buffer (src, piece->piece, piece->buffer);
        }
        break;
      //case lt::piece_finished_alert::alert_type: {
      //    const lt::piece_finished_alert *finished = lt::alert_cast<lt::piece_finished_alert> (a);
          //GST_DEBUG_OBJECT (src, "piece_finished_alert = %u", (guint) (finished->piece_index));
          //src->session->handle.read_piece (finished->piece_index);
      //  }
      //  break;
      }

      auto resume = lt::alert_cast<lt::save_resume_data_alert> (a);
      if (resume) {
        GST_LOG_OBJECT (src, "save_resume_data = %s", resume->message ().c_str ());
        resume->params.ti = std::const_pointer_cast<lt::torrent_info> (src->session->info);
        std::ofstream resume_data (
            (resume->params.save_path + "/." + src->session->info_hash + ".fastresume").c_str (),
            std::ios_base::binary);
        std::vector<char> buf = lt::write_resume_data_buf (resume->params);
        resume_data.write (buf.data(), buf.size());
      }

      auto state_update = lt::alert_cast<lt::state_update_alert> (a);
      if (state_update) {
        GST_LOG_OBJECT (src, "State update %s", state_update->message ().c_str ());
        if (!state_update->status.empty ())
          s = state_update->status.front ();
      }

      if (src->session->error) break;
    }

    g_cond_signal (&src->session_cond);

    g_mutex_unlock (&src->session_mutex);

    g_mutex_lock (&src->cmd_mutex);
    g_cond_wait_until (&src->cmd_cond, &src->cmd_mutex, g_get_monotonic_time () + 500 * G_TIME_SPAN_MILLISECOND);
    g_mutex_unlock (&src->cmd_mutex);

    src->session->ses.post_torrent_updates ();

    if (g_timer_elapsed (src->resume_file_timer, nullptr) > 10.0) {
      src->session->handle.save_resume_data (lt::torrent_handle::only_if_modified);
      g_timer_start (src->resume_file_timer);
    }
  }

  if (s.errc)
    g_set_error_literal (&src->session->error, GST_LIBTORRENT_ERROR, 0, s.errc.message ().c_str ());

  return nullptr;
}

static gboolean
gst_libtorrent_src_pop_piece (GstLibtorrentSrc * src, GstDataQueueItem ** item)
{
  g_mutex_lock (&src->session_mutex);

  if ((gst_data_queue_is_empty (src->session->data_queue) && src->session->rpieces != 0) || src->session->cur_index == *src->session->cur_range.end ())
    src->session->queueing = false;

  GstDataQueueSize level;
  gst_data_queue_get_level (src->session->data_queue, &level);

  if ((level.visible <= 20 /*&& src->session->data_sort.size () >= 20*/ || src->session->npieces <= src->session->rpieces) && !src->session->queueing &&
      src->session->cur_piece.piece + lt::piece_index_t (level.visible) <= src->session->last_piece.piece /*||
      (*src->session->cur_range.begin () == *src->session->cur_range.end () &&
      *src->session->cur_range.end () <= src->session->last_piece.piece)*/) {
    lt::piece_index_t cur_piece, first_piece, last_piece;
    first_piece = *src->session->cur_range.begin ();
    cur_piece = *src->session->cur_range.end ();
    guint npieces = std::min(20u /*- level.visible*/, guint (src->session->last_piece.piece - cur_piece) + 1);
    last_piece = cur_piece + lt::piece_index_t (npieces);
    src->session->cur_range = {first_piece, last_piece};
    src->session->npieces = last_piece - first_piece;
    if (src->session->cur_index < first_piece)
      src->session->cur_index = first_piece;
    if (cur_piece < last_piece) {
      gst_libtorrent_src_read_pieces (src, {cur_piece, last_piece});
      src->session->queueing = true;
    }
  }

  g_mutex_unlock (&src->session_mutex);

  if (!gst_data_queue_pop (src->session->data_queue, item))
    return FALSE;

  g_mutex_lock (&src->session_mutex);

  src->session->cur_range = {reinterpret_cast<GstLibtorrentBuffer *> ((*item)->object)->piece, *src->session->cur_range.end ()};
  --src->session->rpieces;
  if (src->session->cur_index < *src->session->cur_range.begin ())
    src->session->cur_index = *src->session->cur_range.begin ();

  g_mutex_unlock (&src->session_mutex);

  return TRUE;
}

static gssize
gst_libtorrent_src_fill_buffer (GstLibtorrentSrc * src, GstMapInfo & mapinfo, gsize blocksize)
{
  GstDataQueueItem *item;

  g_mutex_lock (&src->session_mutex);

  GstLibtorrentBufferPtr &buf = src->session->cur_buffer, prev;
  const lt::peer_request &last_piece = src->session->last_piece;
  gsize start, length, size, offset = 0;
  lt::peer_request cur_piece = src->session->info->map_file (src->session->file_index,
      src->read_position, std::min (src->piece_length, src->content_size));
  if (cur_piece.piece == last_piece.piece)
    cur_piece.length = last_piece.length;
  cur_piece.length -= cur_piece.start;

  start = cur_piece.start;
  length = cur_piece.length;
  size = std::min (blocksize, length);

  g_mutex_unlock (&src->session_mutex);

  if (cur_piece.piece > buf->piece) {
    if (cur_piece.piece < *src->session->cur_range.begin () ||
        cur_piece.piece >= *src->session->cur_range.end ()) {
      GST_DEBUG_OBJECT (src, "Out of range piece %u", guint (cur_piece.piece));
      return GST_FLOW_ERROR;
    }

    src->session->prev.swap (buf);
    gst_libtorrent_src_drop_until (src->session, cur_piece.piece);

    if (!gst_libtorrent_src_pop_piece (src, &item))
      return GST_FLOW_FLUSHING;
    buf.reset (reinterpret_cast<GstLibtorrentBuffer *> (item->object));
    item->object = nullptr;
    gst_libtorrent_queue_item_free (item);
  }

  g_assert_cmpuint (cur_piece.piece, ==, buf->piece);

  while (offset < blocksize) {
    std::copy_n (buf->buffer.get () + start, size, mapinfo.data + offset);

    if (start + size < src->piece_length || cur_piece.piece == last_piece.piece)
      break;

    offset += size;

    if (!gst_libtorrent_src_pop_piece (src, &item))
      return GST_FLOW_FLUSHING;

    g_mutex_lock (&src->session_mutex);

    cur_piece = src->session->info->map_file (src->session->file_index,
        src->read_position + offset, std::min (src->piece_length, src->content_size));

    g_mutex_unlock (&src->session_mutex);

    prev.swap (buf);
    buf.reset (reinterpret_cast<GstLibtorrentBuffer *> (item->object));
    item->object = nullptr;
    gst_libtorrent_queue_item_free (item);

    if (cur_piece.piece == last_piece.piece)
      cur_piece.length = last_piece.length;
    cur_piece.length -= cur_piece.start;
    if (cur_piece.piece != buf->piece) {
      GST_DEBUG_OBJECT (src, "Different index for current piece %u and piece from queue %u",
          guint (cur_piece.piece), guint (buf->piece));
      return GST_FLOW_ERROR;
    }

    start = cur_piece.start;
    length = cur_piece.length;
    size = std::min (blocksize - offset, length);
  }

  g_mutex_lock (&src->session_mutex);

  src->session->prev = std::move (prev);
  src->session->cur_piece = cur_piece;

  g_mutex_unlock (&src->session_mutex);

  return offset + size;
}

static GstFlowReturn
gst_libtorrent_src_read_buffer (GstLibtorrentSrc * src, GstBuffer ** outbuf)
{
  GstMapInfo mapinfo;
  GstBaseSrc *bsrc;
  gsize blocksize;
  gssize total_read;
  GstFlowReturn ret;
  lt::piece_index_t first_piece, last_piece, start_piece = src->session->cur_piece.piece;

  bsrc = GST_BASE_SRC_CAST (src);
  blocksize = gst_base_src_get_blocksize (bsrc);

  ret = GST_BASE_SRC_CLASS (parent_class)->alloc (bsrc, -1,
      blocksize, outbuf);
  if (G_UNLIKELY (ret != GST_FLOW_OK)) {
    GST_WARNING_OBJECT (src, "Failed to allocate buffer");
    return ret;
  }

  if (!gst_buffer_map (*outbuf, &mapinfo, GST_MAP_WRITE)) {
    GST_WARNING_OBJECT (src, "Failed to map buffer");
    return GST_FLOW_ERROR;
  }

  g_mutex_lock (&src->session_mutex);

  bool cached_buffer (src->session->cur_buffer);

  if (!cached_buffer) {
    GstDataQueueSize level;
    GstDataQueueItem * item;
    gst_data_queue_get_level (src->session->data_queue, &level);
    if (level.visible <= 20 && !src->session->queueing &&
        src->session->cur_piece.piece + lt::piece_index_t (level.visible) <= src->session->last_piece.piece) {
      src->session->npieces = std::min(40 - level.visible, guint32 (src->session->last_piece.piece - src->session->cur_piece.piece) + 1);
      first_piece = src->session->cur_piece.piece;
      last_piece = first_piece + lt::piece_index_t (src->session->npieces);
      src->session->cur_range = {*src->session->cur_range.begin (), last_piece};
      gst_libtorrent_src_read_pieces (src, {first_piece, last_piece});
      src->session->queueing = true;
    }

    g_mutex_unlock (&src->session_mutex);

    if (!gst_libtorrent_src_pop_piece (src, &item))
      return GST_FLOW_FLUSHING;

    src->session->cur_buffer.reset (reinterpret_cast<GstLibtorrentBuffer *> (item->object));
    item->object = nullptr;
    gst_libtorrent_queue_item_free (item);

    g_mutex_lock (&src->session_mutex);
  }

  g_mutex_unlock (&src->session_mutex);

  total_read = gst_libtorrent_src_fill_buffer (src, mapinfo, blocksize);

  if (total_read < 0)
    return GstFlowReturn (total_read);

  GST_TRACE_OBJECT (src, "Read %" G_GSIZE_FORMAT " bytes from pieces %u-%u %" G_GSIZE_FORMAT,
      total_read, (guint) (start_piece), (guint) (src->session->cur_piece.piece), src->read_position);

  src->read_position += total_read;

  if (src->session->error) {
    GST_ERROR_OBJECT (src, "Got error from libtorrent: %s", src->session->error->message);
    g_error_free (src->session->error);
    src->session->error = nullptr;
    gst_buffer_unmap (*outbuf, &mapinfo);
    gst_buffer_unref (*outbuf);
    return GST_FLOW_CUSTOM_ERROR;
  }

  if (total_read < blocksize) {
    //gst_buffer_set_size (*outbuf, total_read);
    mapinfo.size = total_read;
    ret = GST_FLOW_EOS;
  }
  gst_buffer_unmap (*outbuf, &mapinfo);

  g_clear_error (&src->session->error);

  return ret;
}

static GstFlowReturn
gst_libtorrent_src_create (GstPushSrc * psrc, GstBuffer ** outbuf)
{
  GstLibtorrentSrc *src;

  src = GST_LIBTORRENT_SRC (psrc);

  return gst_libtorrent_src_read_buffer (src, outbuf);
}

static gboolean
gst_libtorrent_src_start (GstBaseSrc * bsrc)
{
  GstLibtorrentSrc *src = GST_LIBTORRENT_SRC (bsrc);
  gboolean ret;

  GST_DEBUG_OBJECT (src, "start(\"%s\")", src->location);

  // synchronously receive metadata
  g_mutex_lock (&src->session_mutex);
  ret = gst_libtorrent_src_session_open (src);
  g_mutex_unlock (&src->session_mutex);

  return ret;
}

static gboolean
gst_libtorrent_src_stop (GstBaseSrc * bsrc)
{
  GstLibtorrentSrc *src = GST_LIBTORRENT_SRC (bsrc);

  GST_DEBUG_OBJECT (src, "stop()");

  gst_task_stop (src->task);
  src->session->handle.pause ();

  gst_libtorrent_src_reset (src);

  return TRUE;
}

static GstStateChangeReturn
gst_libtorrent_src_change_state (GstElement * element, GstStateChange transition)
{
  GstStateChangeReturn ret;
  // GstLibtorrentSrc *src;

  // src = GST_LIBTORRENT_SRC (element);

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  return ret;
}

static void
gst_libtorrent_src_set_context (GstElement * element, GstContext * context)
{
  // GstStateChangeReturn ret;
  // GstLibtorrentSrc *src;

  // src = GST_LIBTORRENT_SRC (element);

  GST_ELEMENT_CLASS (parent_class)->set_context (element, context);
}

static gboolean
gst_libtorrent_src_get_size (GstBaseSrc * bsrc, guint64 * size)
{
  GstLibtorrentSrc *src = GST_LIBTORRENT_SRC (bsrc);

  *size = src->content_size;
  GST_DEBUG_OBJECT (src, "get_size() = %" G_GUINT64_FORMAT, *size);
  return src->session != nullptr;
}

static gboolean
gst_libtorrent_src_is_seekable (GstBaseSrc * bsrc)
{
  return TRUE;
}

static void
gst_libtorrent_src_drop_until (GstLibtorrentSession *session, lt::piece_index_t p)
{
  auto it = session->data_sort.begin ();
  GstDataQueueItem *item = nullptr;

  while (it != session->data_sort.end () && (*it)->piece < p) {
    it = session->data_sort.erase (it);
    --session->rpieces;
  }

  gst_data_queue_set_flushing (session->data_queue, TRUE);
  gst_data_queue_set_flushing (session->data_queue, FALSE);
  while (!gst_data_queue_is_empty (session->data_queue) && gst_data_queue_pop (session->data_queue, &item)) {
    auto buf = reinterpret_cast<GstLibtorrentBuffer *> (item->object);
    if (p <= buf->piece) {
      item->object = nullptr;
      session->data_sort.emplace (buf);
    } else {
      //--session->npieces;
      --session->rpieces;
    }
    gst_libtorrent_queue_item_free (item);
  }
}

static gboolean
gst_libtorrent_src_do_seek (GstBaseSrc * bsrc, GstSegment * segment)
{
  GstLibtorrentSrc *src = GST_LIBTORRENT_SRC (bsrc);

  GST_DEBUG_OBJECT (src, "do_seek(%" G_GUINT64_FORMAT "-%" G_GUINT64_FORMAT
      ")", segment->start, segment->stop);
  if (src->read_position == segment->start &&
      src->stop_position == segment->stop) {
    GST_DEBUG_OBJECT (src,
        "Seek to current read/end position and no seek pending");

    g_mutex_lock (&src->session_mutex);

    lt::peer_request cur_piece (src->session->info->map_file (
        src->session->file_index, segment->start, segment->duration));

    if (src->session->prev && cur_piece.piece == src->session->prev->piece) {
      gst_libtorrent_src_drop_until (src->session, cur_piece.piece);
      src->session->cur_range = {cur_piece.piece, *src->session->cur_range.end ()};
      src->session->cur_index = cur_piece.piece;
      src->session->data_sort.insert (std::move (src->session->prev));
      src->session->data_sort.insert (std::move (src->session->cur_buffer));
    }

    g_mutex_unlock (&src->session_mutex);

    return TRUE;
  }

  if (segment->rate < 0.0 || segment->format != GST_FORMAT_BYTES) {
    GST_WARNING_OBJECT (src, "Invalid seek segment");
    return FALSE;
  }

  if (segment->start >= src->content_size) {
    GST_WARNING_OBJECT (src,
        "Potentially seeking behind end of file, might EOS immediately");
  }

  src->read_position = segment->start;
  src->stop_position = segment->stop;

  g_mutex_lock (&src->session_mutex);

  lt::peer_request cur_piece (src->session->info->map_file (
      src->session->file_index, segment->start, segment->duration));

  if (cur_piece.piece < *src->session->cur_range.begin () ||
      cur_piece.piece >= *src->session->cur_range.end ()) {
    if (src->session->prev && cur_piece.piece == src->session->prev->piece) {
      gst_libtorrent_src_drop_until (src->session, cur_piece.piece);
      src->session->cur_range = {cur_piece.piece, *src->session->cur_range.end ()};
      src->session->cur_index = cur_piece.piece;
      src->session->data_sort.insert (std::move (src->session->prev));
      src->session->data_sort.insert (std::move (src->session->cur_buffer));
    } else {
      src->session->prev.reset ();
      src->session->cur_buffer.reset ();
      src->session->cur_piece = cur_piece;
      src->session->cur_index = cur_piece.piece;
      src->session->npieces = 0;
      src->session->rpieces = 0;
      gst_libtorrent_src_cancel_pieces (src, src->session->cur_range);
      gst_data_queue_flush (src->session->data_queue);
      src->session->data_sort.clear();
    }
    src->session->cur_range = {cur_piece.piece, src->session->last_piece.piece};
    src->session->queueing = false;
  } else if (cur_piece.piece > src->session->cur_piece.piece) {
    src->session->cur_buffer.reset ();
    src->session->cur_piece = cur_piece;
    gst_libtorrent_src_cancel_pieces (src, {*src->session->cur_range.begin (), cur_piece.piece});
    gst_libtorrent_src_drop_until (src->session, cur_piece.piece);
    src->session->cur_range = {cur_piece.piece, *src->session->cur_range.end ()};
    src->session->cur_index = cur_piece.piece;
  }

  g_mutex_unlock (&src->session_mutex);

  return TRUE;
}

static GstURIType
gst_libtorrent_src_uri_get_type (GType type)
{
  return GST_URI_SRC;
}

static gboolean
gst_libtorrent_src_query (GstBaseSrc * bsrc, GstQuery * query)
{
  GstLibtorrentSrc *src = GST_LIBTORRENT_SRC (bsrc);
  gboolean ret;
  // GstSchedulingFlags flags;
  // gint minsize, maxsize, align;

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_URI:
      if (gst_uri_is_valid (src->location)) {
        gst_query_set_uri (query, src->location);
      } else {
        gchar *uri = gst_filename_to_uri (src->location, nullptr);
        gst_query_set_uri (query, uri);
        g_free (uri);
      }
      ret = TRUE;
      break;
    default:
      ret = FALSE;
      break;
  }

  if (!ret)
    ret = GST_BASE_SRC_CLASS (parent_class)->query (bsrc, query);

  /* switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_SCHEDULING:
      gst_query_parse_scheduling (query, &flags, &minsize, &maxsize, &align);
      flags |= GST_SCHEDULING_FLAG_BANDWIDTH_LIMITED;
      gst_query_set_scheduling (query, flags, minsize, maxsize, align);
      break;
    default:
      break;
  } */

  return ret;
}

static const gchar *const *
gst_libtorrent_src_uri_get_protocols (GType type)
{
  static const gchar *protocols[] = { "magnet", nullptr };

  return protocols;
}

static gchar *
gst_libtorrent_src_uri_get_uri (GstURIHandler * handler)
{
  GstLibtorrentSrc *src = GST_LIBTORRENT_SRC (handler);

  return g_strdup (src->location);
}

static gboolean
gst_libtorrent_src_uri_set_uri (GstURIHandler * handler, const gchar * uri,
    GError ** error)
{
  GstLibtorrentSrc *src = GST_LIBTORRENT_SRC (handler);

  lt::error_code ec;
  lt::parse_magnet_uri (uri, ec);
  if (ec) {
    g_set_error_literal (error,
        GST_LIBTORRENT_ERROR,
        ec.value (),
        ec.message ().c_str ());
    return FALSE;
  }

  src->location = g_strdup (uri);
  if (src->session)
      src->session->location = lt::string_view (src->location);

  return TRUE;
}

static void
gst_libtorrent_src_uri_handler_init (gpointer g_iface, gpointer iface_data)
{
  auto iface = reinterpret_cast<GstURIHandlerInterface *> (g_iface);

  iface->get_type = gst_libtorrent_src_uri_get_type;
  iface->get_protocols = gst_libtorrent_src_uri_get_protocols;
  iface->get_uri = gst_libtorrent_src_uri_get_uri;
  iface->set_uri = gst_libtorrent_src_uri_set_uri;
}

static gboolean
libtorrentsrc_element_init (GstPlugin * plugin)
{
  gboolean ret = TRUE;

  GST_DEBUG_CATEGORY_INIT (libtorrentsrc_debug, "libtorrentsrc", 0,
      "libtorrent BitTorrent src");

  libtorrent_element_init (plugin);

  ret = gst_element_register (plugin, "libtorrentsrc",
      GST_RANK_PRIMARY, GST_TYPE_LIBTORRENT_SRC);

  return ret;
}
