/*
 * GStreamer
 * Copyright (C) 2023 Martin Reboredo <yakoyoku@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __GST_LIBTORRENT_SRC_H
#define __GST_LIBTORRENT_SRC_H

#include <gst/base/gstpushsrc.h>

G_BEGIN_DECLS
#define GST_TYPE_LIBTORRENT_SRC \
  (gst_libtorrent_src_get_type())
#define GST_LIBTORRENT_SRC(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_LIBTORRENT_SRC,GstLibtorrentSrc))
#define GST_LIBTORRENT_SRC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), \
    GST_TYPE_LIBTORRENT_SRC,GstLibtorrentSrcClass))
#define GST_IS_LIBTORRENT_SRC(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_LIBTORRENT_SRC))
#define GST_IS_LIBTORRENT_SRC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_LIBTORRENT_SRC))
typedef struct _GstLibtorrentSrc GstLibtorrentSrc;
typedef struct _GstLibtorrentSrcClass GstLibtorrentSrcClass;

typedef struct _GstLibtorrentSession GstLibtorrentSession;

struct _GstLibtorrentSrc
{
  GstPushSrc element;

  GstLibtorrentSession *session;
  gchar *location;
  gchar *user_agent;
  gchar *file_path;
  gchar *save_path;

  gchar *listen_url;

  gchar **file_list;

  GstTask *task;

  GTimer *resume_file_timer;

  GMutex session_mutex;
  GCond session_cond;
  GRecMutex task_mutex;

  GMutex cmd_mutex;
  GCond cmd_cond;

  gsize content_size;
  gsize piece_length;
  guint64 read_position;
  guint64 request_position;
  guint64 stop_position;
};

struct _GstLibtorrentSrcClass
{
  GstPushSrcClass parent_class;

  void (*file_list) (GstLibtorrentSrc *src, const gchar* const* files);
};

GType gst_libtorrent_src_get_type (void);

GST_ELEMENT_REGISTER_DECLARE (libtorrentsrc);

G_END_DECLS
#endif /* __GST_LIBTORRENT_SRC_H */
