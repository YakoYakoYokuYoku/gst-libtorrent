# GStreamer libtorrent

:warning: :construction: **WORK IN PROGRESS! NOT READY FOR USE YET!**

This is a GStreamer BitTorrent streaming plugin based on [libtorrent](https://libtorrent.org).

## Requierments

You need [libtorrent](https://github.com/arvidn/libtorrent), meson and gst-plugins-base packages.

## Building

Simply run:

```
meson setup builddir
meson compile -C builddir
```

This will automatically create the `builddir` directory and build this project inside it.
